/* eslint no-console: 0 */

var logger = require('../src');

try {
  logger.configure({
    'logger': {
      'info':  { 'active': true, 'color': 'green' },
      'warn':  { 'active': true, 'color': 'yellow' },
      'error': { 'active': true, 'color': 'red' },
      'trace': { 'active': true, 'color': 'blue' },
      'debug': { 'active': true, 'color': 'magenta' }
    }
  });
  const { info, warn, error, trace, debug } = logger.init(__filename);
  if (info === null) throw new Error('Info function is undefined');
  if (typeof info !== 'function') throw new Error('Info object is not a function');
  if (warn === null) throw new Error('Warn function is undefined');
  if (typeof warn !== 'function') throw new Error('Warn object is not a function');
  if (error === null) throw new Error('Error function is undefined');
  if (typeof error !== 'function') throw new Error('Error object is not a function');
  if (trace === null) throw new Error('Trace function is undefined');
  if (typeof trace !== 'function') throw new Error('Trace object is not a function');
  if (debug === null) throw new Error('Debug function is undefined');
  if (typeof debug !== 'function') throw new Error('Debug object is not a function');

  info('Hello World');
  warn('Hello World');
  error('Hello World');
  trace('Hello World');
  debug('Hello World');

  info(1);
  info(0);
  info(-1);
  warn(1);
  warn(0);
  warn(-1);
  warn(0);
  error(0);
  error(-1);
  error(0);

  info(null);
  warn(null);
  error(null);

  info(undefined);
  warn(undefined);
  error(undefined);

  var j = {fName: 'Matt', lname: 'Felske'};
  info(JSON.stringify(j, null, '\t'));

  info({fName: 'Matt', lname: 'Felske'});
  warn({fName: 'Matt', lname: 'Felske'});
  error({fName: 'Matt', lname: 'Felske'});
  error(new Error('my name is undefined'));

  info('Hello World', -1);
  info('Hello World', null);
  info('Hello World', undefined);
  info('Hello World', j);


  console.log('Passed 6 of 6.');
  process.exit(0);
} catch (err) {
  console.error(`Error: ${err.message}`);
  console.error(err);
  process.exit(1);
}
