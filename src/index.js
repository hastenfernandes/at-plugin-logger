const colors = require('colors');
const tracer = require('tracer');

var logger = null;
var options = null;

const init = (fileName) => {

  const logArguments = (logger, argumentA, argumentB) => {
    if (argumentB !== undefined) {
      logger(`${fileName} - ${processArgument(argumentA)} ${processArgument(argumentB)}`, argumentA, argumentB);
    } else {
      logger(`${fileName} - ${processArgument(argumentA)}`, argumentA);
    }
  };

  const processArgument = (arg) => {
    switch (typeof arg) {
      case 'string':
        return '%s';
      case 'number':
        return '%d';
      case 'object':
        return '';
      default:
        return '';
    }
  };

  const info = (argumentA, argumentB) => {
    if (!options.logger.info.active) {
      return;
    }
    logArguments(logger.info, argumentA, argumentB);
  };

  const warn = (argumentA, argumentB) => {
    if (!options.logger.warn.active) {
      return;
    }
    logArguments(logger.warn, argumentA, argumentB);
  };

  const error = (argumentA, argumentB) => {
    if (!options.logger.error.active) {
      return;
    }
    logArguments(logger.error, argumentA, argumentB);
  };

  const trace = (argumentA, argumentB) => {
    if (!options.logger.trace.active) {
      return;
    }
    logArguments(logger.trace, argumentA, argumentB);
  };

  const debug = (argumentA, argumentB) => {
    if (!options.logger.debug.active) {
      return;
    }
    logArguments(logger.debug, argumentA, argumentB);
  };

  return { info, warn, error, trace, debug };
};

module.exports = {
  configure: (opt) => {
    options = opt;

    logger = tracer['colorConsole']({
      stackIndex: 2,
      format:     '{{timestamp}} [{{title}}] line {{line}} {{message}}',
      filters:    {
        trace: colors[options.logger.trace.color],
        debug: colors[options.logger.debug.color],
        info:  colors[options.logger.info.color],
        warn:  colors[options.logger.warn.color],
        error: [ colors[options.logger.error.color], colors.bold ]
      }
    });
  },
  init
};
